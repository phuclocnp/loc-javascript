function first(doSomething) {
  doSomething()
}

function second(viecnaodo) {
  doSomething()
}

function third(doSomething) {
  doSomething()
}

// Code không tối ưu
function main() {
  first(function() {
    second(function() {
      third(function() {
        console.log("OMG!!!!")
      })
    })
  })
}
