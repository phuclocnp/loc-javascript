const MIN = 0;
const MAX = 36;

var testNumber = 15;
var i = 1;

while (MAX) {
  let randomValue = Math.floor(Math.random() * (MAX - MIN) + MIN);
  if (randomValue == testNumber) {
    break;
  }
  console.log('Round' + i + ': ' + randomValue);
  i++;
}

console.log('The script went ' + i + ' rounds before finding ' + testNumber + '.');

let j = 500;
var reps = 0;
do {
  ++reps;
  console.log(reps + ' reps gives us ' + j);
  j*=2.1;
} while (j < 500);