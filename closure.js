function giveMeEms(pixels) {
  var baseValue = 16;

  function doTheMath() {
      return pixels / baseValue;
  }

  return doTheMath;
}

var smallSize = giveMeEms(12);
var mediumSize = giveMeEms(18);

console.log('Small size: ', smallSize());
console.log('Medium size: ', mediumSize());